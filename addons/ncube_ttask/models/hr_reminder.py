from odoo import models, fields, api

class Reminder(models.Model):
    _name = 'hr.reminder'
    _description = 'reminder for birthdays'

    recipient = fields.Many2one(
        comodel_name='res.partner',
        string='Recipient',
        required=True)
    days_before = fields.Integer(
        string='days to remind before birthday',
        default=1,
        required=True)

    @api.onchange('days_before')
    def days_before(self):
        rec = self.env['hr.employee'].search([
            ('address_home_id', '=', self.recipient.id)
        ], limit=1)
        rec.write(
            {
                'days_before': self.days_before
            }
        )

    # @api.onchange('recipient')
    # def _onchange_recipient(self):
    #     rec = self.env['hr.reminder'].search([
    #         ('recipient', '=', self.address_home_id.id)
    #     ], limit=1)
    #     if rec:
    #         res = {'warning': {'title': _('Warning'), 'message': _('Recipient already exist')}}
    #         return res