import datetime
import operator
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models
import logging
_logger = logging.getLogger(__name__)

class NCubeEmployee(models.Model):
    _inherit = 'hr.employee'

    remind = fields.Boolean(
        string='Include in Birthday Reminder List',
        default=False)
    next_bd = fields.Date(
        string='Next Birthday',
        compute='_compute_next_bd',
        search='_search_next_bd')
    remind_bd = fields.Date(
        string='Birthday Remind Date',
        compute='_compute_next_bd',
        search='_search_remind_bd')
    days_before = fields.Integer(
        string='days to remind before birthday',
        default=1,
        required=True)

    def _search_next_bd(self, operator_odoo, value):
        month = int(value.split('-')[1])
        day = int(value.split('-')[2])
        ops = {"<=": operator.le,
               ">=": operator.ge,
               '>': operator.gt,
               '<': operator.lt,
               '=': operator.eq,
               }
        emp_bb = self.env['hr.employee'].search([('remind', '=', True)])
        if '=' not in operator_odoo:
            emp_ids = emp_bb.filtered(lambda emp: ops[operator_odoo + '='](emp.birthday.month, month))
        else:
            emp_ids = emp_bb.filtered(lambda emp: ops[operator_odoo](emp.birthday.month, month))
        def second_filter(f):
            if f.birthday.month == month:
                return ops[operator_odoo](f.birthday.day, day)
            else:
                return True
        emp_idsf = emp_ids.filtered(lambda emp: second_filter(emp))
        return [('id', 'in', emp_idsf.ids)]

    def _search_remind_bd(self, operator_odoo, value):
        month = int(value.split('-')[1])
        day = int(value.split('-')[2])
        ops = {"<=": operator.le,
               ">=": operator.ge,
               '>': operator.gt,
               '<': operator.lt,
               '=': operator.eq,
               }
        emp_bb = self.env['hr.employee'].search([('remind', '=', True)])
        if '=' not in operator_odoo:
            emp_ids = emp_bb.filtered(lambda emp: ops[operator_odoo + '='](emp.birthday.month, month))
        else:
            emp_ids = emp_bb.filtered(lambda emp: ops[operator_odoo](emp.birthday.month, month))
        def second_filter(f):
            if f.birthday.month == month:
                return ops[operator_odoo](f.birthday.day, day + f.days_before)
            else:
                return True
        emp_idsf = emp_ids.filtered(lambda emp: second_filter(emp))
        return [('id', 'in', emp_idsf.ids)]

    @api.depends('birthday')
    def _compute_next_bd(self):
        self.next_bd = False
        self.remind_bd = False
        if not self.birthday:
            return
        today = datetime.date.today()
        before_bd = self.days_before
        if self.birthday.month < today.month:
            self.next_bd = datetime.date(year=today.year + 1, day=self.birthday.day, month=self.birthday.month)
            self.remind_bd = self.next_bd - relativedelta(days=before_bd)
            return
        elif self.birthday.month == today.month and self.birthday.day <= today.day:
            self.next_bd = datetime.date(year=today.year + 1, day=self.birthday.day, month=self.birthday.month)
            self.remind_bd = self.next_bd - relativedelta(days=before_bd)
            return
        self.next_bd = datetime.date(year=today.year, day=self.birthday.day, month=self.birthday.month)
        self.remind_bd = self.next_bd - relativedelta(days=before_bd)

    @api.onchange('remind', 'birthday')
    @api.depends('remind', 'birthday')
    def _onchange_remind(self):
        env = self.env['hr.reminder']
        if self.remind and self.birthday:
            rec = env.search([
                ('recipient', '=', self.address_home_id.id)
            ])
            if rec:
                rec[0].write(
                    {
                        'days_before': self.days_before
                    }
                )
            else:
                env.create({
                    'recipient': self.address_home_id.id,
                    'days_before': self.days_before
                })